use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::glib;
use glib::{ParamSpec, ParamSpecUInt, subclass::Signal};
use once_cell::sync::{OnceCell, Lazy};

use crate::backend::sudoku::Sudoku;
use crate::frontend::sudoku_grid::sudoku_cell::SudokuCell;

#[derive(Default)]
pub struct SudokuGrid {
    pub sudoku_cells: OnceCell<Vec<Vec<SudokuCell>>>,
    pub sudoku: OnceCell<Sudoku>,
    num_error: std::cell::Cell<u32>,
}

impl ObjectImpl for SudokuGrid {
    fn constructed(&self) {
        self.parent_constructed();

        let obj = self.obj();
        obj.setup_sudoku();
        obj.build_ui();
    }

    // Properties
    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: Lazy<Vec<ParamSpec>> =
            Lazy::new(|| vec![ParamSpecUInt::builder("num-error").build()]);
        
        PROPERTIES.as_ref()   
    }

    fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
        match pspec.name() {
            "num-error" => {
                let val =
                    value.get().expect("The value needs to be of the type `u32`");
                
                self.num_error.replace(val);
            }
            _ => unimplemented!(),
        }
    }

    fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
        match pspec.name() {
            "num-error" => self.num_error.get().to_value(),
            _ => unimplemented!(),
        }
    }

    // Signal
    fn signals() -> &'static [glib::subclass::Signal] {
        static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
            vec![Signal::builder("num-error-changed")
                .param_types([u32::static_type()])
                .build()]
        });

        SIGNALS.as_ref()
    }
}

#[glib::object_subclass]
impl ObjectSubclass for SudokuGrid {
    const NAME: &'static str = "SudokuGrid";
    type Type = super::SudokuGrid;
    type ParentType = adw::Bin;
}

impl WidgetImpl for SudokuGrid {}

impl BinImpl for SudokuGrid {}

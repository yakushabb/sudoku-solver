use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{glib, CompositeTemplate, Button};
use glib::subclass::InitializingObject;
use once_cell::sync::OnceCell;

use crate::frontend::sudoku_grid::SudokuGrid;

#[derive(CompositeTemplate, Default)]
#[template(resource = "/io/gitlab/cyberphantom52/sudoku_solver/ui/sudoku_row.ui")]
pub struct SudokuRow {
    #[template_child]
    pub btn_delete: TemplateChild<Button>,
    
    pub sudoku_grid: OnceCell<SudokuGrid>,
}

#[glib::object_subclass]
impl ObjectSubclass for SudokuRow {
    const NAME: &'static str = "SudokuRow";
    type Type = super::SudokuRow;
    type ParentType = adw::ActionRow;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for SudokuRow {
    fn constructed(&self) {
        self.parent_constructed();
        
        let obj = self.obj();
        obj.setup_actions();
    }
}

impl WidgetImpl for SudokuRow {}

impl ActionRowImpl for SudokuRow {}

impl PreferencesRowImpl for SudokuRow {}

impl ListBoxRowImpl for SudokuRow {}

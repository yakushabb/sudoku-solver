use rand::seq::SliceRandom;
use super::sudoku::Sudoku;

pub fn wave_function_collapse(sudoku: &Sudoku) {
    generate_entropy(sudoku);
    while let Some((row, column)) = get_lowest_entropy_cell(sudoku) {
        let cell = sudoku.get(row, column);

        if let Some(value) = cell.possible_values()
            .choose(&mut rand::thread_rng()) 
        {
            cell.set_val(*value);
            propagate(sudoku, row, column);   
        }
    }
}

/// This function returns a random cell position among the cells with the lowest entropy.
fn get_lowest_entropy_cell(sudoku: &Sudoku) -> Option<(usize, usize)> {
    let mut min_entropy = 10;
    let mut cells_with_min_entropy: Vec<(usize, usize)> = Vec::new();
    
    for row in 0..9 {
        for column in 0..9 {
            let cell = sudoku.get(row, column);
            if cell.get_val() != 0 || cell.get_entropy() == 0 {continue};

            let entropy = cell.get_entropy();

            if entropy < min_entropy {
                min_entropy = entropy;
                cells_with_min_entropy.clear();
                cells_with_min_entropy.push((row, column));
            } else if entropy == min_entropy {
                cells_with_min_entropy.push((row, column));
            }
        }
    }

    cells_with_min_entropy
        .choose(&mut rand::thread_rng())
        .copied()
}

/// Propagates the entropy change to the cells in the same row, column and block.
fn propagate(sudoku: &Sudoku, row: usize, column: usize) {
    let value = sudoku.get(row, column).get_val();
    let box_row_start = (row / 3) * 3;
    let box_col_start = (column / 3) * 3;
    for i in 0..9 {
        sudoku.get(row, i).remove_entropy(value);

        sudoku.get(i, column).remove_entropy(value);

        sudoku.get(box_row_start + i / 3, box_col_start + i % 3)
            .remove_entropy(value);
    }
}

/// Generate entropy for each cell.
fn generate_entropy(sudoku: &Sudoku) {
    for row in 0..9 {
        for column in 0..9 {
            let cell = sudoku.get(row, column);
            if cell.get_val() == 0 {continue};

            propagate(sudoku, row, column);
        }
    }
}
